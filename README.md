# OrangeStore

## Sobre nós
### Nome
> OrangeStore Fabrica de Gomos SA
### Localização
> Padre Nobrega, Marília - SP
### Site
> orange.store
### Ramo de atividade
> Produção e venda de gomos de laranja
### Quem somos nós?
> Criada em 2001, a **OrangeStore** é uma empresa voltada à produção e venda de gomos de laranja de várias cores e sabores.

## Descrição dissertativa
A **OrangeStore** verificou a necessidade de facilitar a compra e venda de seus gomos para seus clientes. Usando a estrutura atual de venda, representante comercial (RC), as compras são realizadas pelos clientes através do RC e o mesmo realiza o pedido para a **OrangeStore**.

Afim de simplificar e agilizar este processo decidiu-se criar uma interface onde o próprio cliente possa realizar seus pedidos, os quais poderão ser acompanhados pelo representante.

O cliente poderá utilizar os seguintes recursos:

* Solicitação de conta;
* Montagem de pedido;
* Compra;
* Acompanhamento do pedido;
* Lista de produtos;

O representante poderá utilizar os seguintes recursos:

* Confirmação de contas;
* Acompanhamento de pedidos;
* Frequência de pedidos dos clientes;
* Relatório de vendas;

```plantuml

hide footbox

actor Cliente
boundary Serviço
actor Representante

Cliente -> Serviço : Solicita conta
Serviço -> Representante : Envia solicitação de conta
Representante -> Cliente : Aceita/Recusa solicitação
Serviço -> Cliente : Mostra Lista de Produtos
Cliente -> Serviço : Monta Pedido
Serviço -> Representante : Mostra lista de pedidos
Cliente -> Serviço : Realiza Compra
Cliente -> Serviço : Solicita acompanhamento do pedido
Serviço -> Cliente : Status do pedido
Serviço -> Representante : Relatório de venda
Serviço -> Representante : Frequencia de pedidos por cliente

```