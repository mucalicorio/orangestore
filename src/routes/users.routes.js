const express = require('express');
const router = express.Router();
const userController = require('../controllers/users.controller');

router.get('/:id', userController.getUser);

router.post('/', userController.insertUser);

module.exports = router;
