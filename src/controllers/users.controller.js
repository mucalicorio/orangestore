require('dotenv/config');

const mysql = require('../mysql');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

exports.getUser = (req, res, next) => {
    mysql.getConnection((error, connection) => {
        if (error) return res.status(500).send({ error: error });
        connection.query(
            `SELECT * FROM users WHERE id = ${req.params.id}`, (error, result) => {
                connection.release();
                if (error) return res.status(500).send({ error: error });
                return res.status(200).send(result[0]);
            }
        );
    });
}

exports.insertUser = (req, res, next) => {
    mysql.getConnection((error, connection) => {
        if (error) {
            return res.status(500).send({ error: error });
        }

        connection.query(`SELECT 1 FROM users WHERE cnpj = ${req.body.cnpj}`, (error, results) => {
            if (error) {
                return res.status(500).send({ error: error });
            }
            if (results.length > 0) {
                return res.status(500).send({ error: 'User already exists' });
            }

            bcrypt.hash(req.body.password, 10, (errorBcrypt, hash) => {
                if (errorBcrypt) {
                    return res.status(500).send({ error: errorBcrypt });
                }

                connection.query(
                    `
                        INSERT INTO users 
                            (cnpj, password, fullname, address, zipcode, city, state, country, email, zones_id) 
                        VALUES
                            ('${req.body.cnpj}', '${hash}', '${req.body.fullname}', '${req.body.address}',
                             '${req.body.zipcode}', '${req.body.city}', '${req.body.state}', '${req.body.country}',
                             '${req.body.email}', ${req.body.zones_id}
                            )
                    `, (error, result) => {
                    connection.release();
                    if (error) {
                        return res.status(500).send({ error: error });
                    }

                    let token = jwt.sign({
                        id: result.insertId,
                        fullname: req.body.fullname,
                        zones_id: req.body.zones_id
                    }, process.env.SAL, {});

                    return res.status(201).send({
                        token: token
                    });
                }
                )
            });
        });
    });
}
